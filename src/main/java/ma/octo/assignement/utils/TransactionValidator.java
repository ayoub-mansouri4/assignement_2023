package ma.octo.assignement.utils;


import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;


@Slf4j
public class TransactionValidator {
    private static final int MONTANT_MAXIMAL = 10000;
    private static final int MONTANT_MINIMAL = 100;

    public static void validateTransfert(Account senderAccount, Account recieverAccount, TransferDto transferDto)throws InsuffisantBalanceException, AccountNotFoundException, TransactionException{

        if (senderAccount == null) {
            log.error("Issuer account Non-existent");
            throw new AccountNotFoundException("Issuer account Non-existent");
        }

        if (recieverAccount == null) {
            log.error("Receiver Account Non-existent");
            throw new AccountNotFoundException("Receiver Account Non-existent");
        }

        if (transferDto.getMontant() == null || transferDto.getMontant().intValue() == 0) {
            log.error("Empty amount");
            throw new TransactionException("Empty amount");

        } else if (transferDto.getMontant().intValue() < MONTANT_MINIMAL) {
            log.error("Minimum transfer amount not reached");
            throw new TransactionException("Minimum transfer amount not reached");


        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            log.error("Maximum transfer amount exceeded");
            throw new TransactionException("Maximum transfer amount exceeded");
        }

        if (transferDto.getMotif() == null || transferDto.getMotif().length() <1) {
            log.error("invalid Reason");
            throw new TransactionException("invalid Reason");
        }

        if (senderAccount.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            log.error("Insufficient issuer balance");
            throw new InsuffisantBalanceException();
        }
    }

    public static void validateDeposit(Account account, DepositDto depositDto) throws TransactionException,AccountNotFoundException{

        if (account==null){
            log.warn("Non-existent account");
            throw new AccountNotFoundException();
        }
        if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL){
            log.warn("Maximum transfer amount exceeded");
            throw new TransactionException("Maximum transfer amount exceeded");
        }

        if (StringUtils.isEmpty(depositDto.getNomPrenomEmetteur())){
            log.warn("Issuer name does not exist");
            throw new TransactionException("Issuer name does not exist");
        }

        if (StringUtils.isEmpty(depositDto.getMotif())) {
            log.warn("invalid Reason");
            throw new TransactionException("invalid Reason");
        }
    }
}
