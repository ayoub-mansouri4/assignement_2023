package ma.octo.assignement.service;



import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.utils.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
@Slf4j
@AllArgsConstructor
public class DepositServiceImpl implements DepositService{;
    AuditService auditService;
    DepositRepository depositRepository;
    AccountRepository accountRepository;


    @Override
    public void createDeposit(DepositDto depositDto) throws AccountNotFoundException, TransactionException {
        Account account = accountRepository.findByRib(depositDto.getCompteBeneficiaire().getRib());
       TransactionValidator.validateDeposit(account,depositDto);

        account.setSolde(account.getSolde().add(depositDto.getMontant()));

        Deposit deposit = DepositMapper.map(depositDto,account);

        depositRepository.save(deposit);

        auditService.deposit(depositDto);

    }

    @Override
    public List<Deposit> getAll() {
        return depositRepository.findAll();
        }
}
