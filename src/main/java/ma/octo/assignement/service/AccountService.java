package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.exceptions.AccountNotFoundException;

import java.util.List;

public interface AccountService {
    List<Account> getAccounts();

    Account save(Account account);

    Account getAccount(long id);

    Account update(Account account) throws AccountNotFoundException;

}
