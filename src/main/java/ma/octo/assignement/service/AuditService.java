package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;

public interface AuditService {
    void transfer(TransferDto transferDto);
    void deposit(DepositDto depositDto);
}
