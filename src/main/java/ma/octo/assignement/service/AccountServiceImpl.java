package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {

    AccountRepository accountRepository;


    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account getAccount(long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    public Account update(Account account) throws AccountNotFoundException {
        Account account1 = null;

        if (account!=null && account.getId()!=0){
            account1 = accountRepository.findById(account.getId()).get();

            if (account1 != null){
                account1.setSolde(account.getSolde()!=null?account.getSolde():account1.getSolde());
            }else {
                throw new AccountNotFoundException();
            }
        }else {
            throw new AccountNotFoundException();
        }

        return account1;

    }


    @Override
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }
}
