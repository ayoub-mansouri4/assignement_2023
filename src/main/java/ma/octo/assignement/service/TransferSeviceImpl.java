package ma.octo.assignement.service;


import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.utils.TransactionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


@Transactional
@Service
@AllArgsConstructor
public class TransferSeviceImpl implements TransferService{
    AuditService auditService;
    TransferRepository transferRepository;
    AccountRepository accountRepository;



    @Override
    public List<Transfer> getAll() {
        return transferRepository.findAll();
    }

    public void createTransfer(TransferDto transferDto)
            throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {

        Account senderAccount = accountRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Account recieverAccount = accountRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        TransactionValidator.validateTransfert(senderAccount ,recieverAccount,transferDto);

        senderAccount.setSolde(senderAccount.getSolde().subtract(transferDto.getMontant()));

        recieverAccount.setSolde(new BigDecimal(recieverAccount.getSolde().intValue() + transferDto.getMontant().intValue()));

        Transfer transfer = TransferMapper.map(transferDto,recieverAccount,senderAccount);

        transferRepository.save(transfer);

        auditService.transfer(transferDto);
    }
}
