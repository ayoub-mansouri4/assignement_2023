package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;


public interface TransferService {

    List<Transfer> getAll();
    void createTransfer(TransferDto transferDto) throws InsuffisantBalanceException, AccountNotFoundException, TransactionException;
}
