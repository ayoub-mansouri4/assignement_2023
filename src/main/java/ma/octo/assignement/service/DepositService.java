package ma.octo.assignement.service;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface DepositService {
    void createDeposit(DepositDto depositDto) throws AccountNotFoundException, TransactionException;

    List<Deposit> getAll();
}
