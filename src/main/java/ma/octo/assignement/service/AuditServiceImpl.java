package ma.octo.assignement.service;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.utils.EventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class AuditServiceImpl implements AuditService{

    private AuditRepository auditRepository;

    public void transfer(TransferDto transferDto) {
        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage("Transfer from " + transferDto.getNrCompteEmetteur() + " to " + transferDto
                .getNrCompteBeneficiaire() + " for an amount of " + transferDto.getMontant()
                .toString());
        auditRepository.save(audit);
    }


    public void deposit(DepositDto deposit) {
        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage("Deposit to account " + deposit.getCompteBeneficiaire().getNrCompte() + " by " + deposit
                .getNomPrenomEmetteur() + " for an amount of " + deposit.getMontant()
                .toString());
        auditRepository.save(audit);
    }
}
