package ma.octo.assignement.web.controller;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/transfer")
@AllArgsConstructor
public class TransferController {

    private TransferService transferService;

    @GetMapping("/all")
    List<Transfer> getTransfers() {
        return transferService.getAll();
    }


    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {
       transferService.createTransfer(transferDto);
    }
}
