package ma.octo.assignement.web.controller;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RequestMapping("/account")
@RestController
@AllArgsConstructor
public class AccountController {

    AccountService accountService;


    @GetMapping("/all")
    List<Account> getAccounts() {
        return accountService.getAccounts();
    }

    @GetMapping("/{id}")
    Account getAccount(@PathVariable("id") long id) {
        return accountService.getAccount(id);
    }


    @PostMapping("/create")
    Account createAccount(@RequestBody Account account){
        return accountService.save(account);
    }


    @PutMapping("/update")
    Account updateAccount(@RequestBody Account account) throws AccountNotFoundException {
        return accountService.update(account);
    }
}
