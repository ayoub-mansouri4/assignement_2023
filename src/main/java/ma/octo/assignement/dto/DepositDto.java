package ma.octo.assignement.dto;

import lombok.Data;
import ma.octo.assignement.domain.Account;

@Data
public class DepositDto extends TransactionDto{
    private String nomPrenomEmetteur;
    private Account compteBeneficiaire;
}
