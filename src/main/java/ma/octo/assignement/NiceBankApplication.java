package ma.octo.assignement;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TransferRepository transferRepository;

	@Autowired
	private DepositRepository depositRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		User user1 = new User();
		user1.setUsername("user1");
		user1.setLastname("last1");
		user1.setFirstname("first1");
		user1.setGender("Male");

		userRepository.save(user1);


		User utilisateur2 = new User();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		userRepository.save(utilisateur2);

		Account account1 = new Account();
		account1.setNrCompte("010000A000001000");
		account1.setRib("RIB1");
		account1.setSolde(BigDecimal.valueOf(200000L));
		account1.setUser(user1);
		accountRepository.save(account1);

		Account account2 = new Account();
		account2.setNrCompte("010000B025001000");
		account2.setRib("RIB2");
		account2.setSolde(BigDecimal.valueOf(140000L));
		account2.setUser(utilisateur2);
		accountRepository.save(account2);

		Transfer v = new Transfer();
		v.setMontantTransfer(BigDecimal.TEN);
		v.setCompteBeneficiaire(account2);
		v.setCompteEmetteur(account1);
		v.setDateExecution(new Date());
		v.setMotifTransfer("Assignment 2021");
		transferRepository.save(v);

		Deposit d = new Deposit();
		d.setNomPrenomEmetteur("Ayoub Mansouri");
		d.setMontant(BigDecimal.valueOf(1000));
		d.setCompteBeneficiaire(account1);
		d.setMotifDeposit("Rien");
		d.setDateExecution(new Date());
		depositRepository.save(d);
		
	}
}
