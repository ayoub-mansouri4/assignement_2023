package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.BDDMockito.given;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DepositServiceTest {

    private Deposit deposit1;
    private Account account1;
    private User user1;

    private List<Deposit> depositList;


    @InjectMocks
    private DepositServiceImpl depositService;

    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    AccountRepository accountRepository;



    @BeforeEach
    void initialize(){
        user1 = new User();
        user1.setUsername("AyoubMns1");
        user1.setLastname("Ayoub1");
        user1.setFirstname("Mansouri1");
        user1.setGender("Male");

        account1 = new Account();
        account1.setId(1L);
        account1.setNrCompte("010000A000001000");
        account1.setRib("RIB1");
        account1.setSolde(BigDecimal.valueOf(200000L));
        account1.setUser(user1);


        deposit1 = new Deposit();
        deposit1.setMotifDeposit("Rien");
        deposit1.setMontant(BigDecimal.valueOf(2000L));
        deposit1.setCompteBeneficiaire(account1);
        deposit1.setNomPrenomEmetteur("Ayoub2");
        deposit1.setDateExecution(new Date());



    }

    @AfterEach
    void destroy(){
        account1=null;
        user1 =null;
        deposit1 = null;
    }

    @DisplayName("It Should raise an AccountNotFoundException since no account is given")
    @Test
    public void testDepositAccountNotFoundException(){
        when(accountRepository.findByRib(any())).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit1));
        });

        assertEquals(accountNotFoundException.getMessage(),"Non-existent account");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is higher than the specified one")
    @Test
    public void testDepositTransactionExceedTransactionMoneyException(){
        when(accountRepository.findByRib(any())).thenReturn(account1);
        deposit1.setMontant(BigDecimal.valueOf(2000000L));
        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit1));
        });

        assertEquals(transactionException.getMessage(),"Maximum transfer amount exceeded");

    }


    @DisplayName("It Should raise an TransactionException since the issuer name is empty")
    @Test
    public void testDepositTransactionMotifException(){
        when(accountRepository.findByRib(any())).thenReturn(account1);

        deposit1.setNomPrenomEmetteur("");

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit1));
        });

        assertEquals(transactionException.getMessage(),"Issuer name does not exist");

    }

    @DisplayName("It Should Increase Reciever Amount")
    @Test
    public void testDepositShouldIncreaseRecieverAmount() throws Exception{
        when(accountRepository.findByRib(any())).thenReturn(account1);
            BigDecimal account1ExpectedAmount = account1.getSolde().add(deposit1.getMontant());
            depositService.createDeposit(DepositMapper.map(deposit1));
            assertEquals(account1ExpectedAmount, deposit1.getCompteBeneficiaire().getSolde());

    }
    @DisplayName("It Should Test GetAllDeposits Method")
    @Test
    public void testDepositShouldGetAllDeposits(){
        Deposit deposit2 = new Deposit();
        deposit2.setMotifDeposit("Rien");
        deposit2.setMontant(BigDecimal.valueOf(2000L));
        deposit2.setCompteBeneficiaire(account1);
        deposit2.setNomPrenomEmetteur("Ayoub2");
        deposit2.setDateExecution(new Date());

        given(depositService.getAll()).willReturn(List.of(deposit1,deposit2));

        depositList=depositService.getAll();
        Assertions.assertNotEquals(null,depositList);
        Assertions.assertEquals(2,depositList.size());


    }






}
