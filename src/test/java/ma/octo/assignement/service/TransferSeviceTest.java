package ma.octo.assignement.service;


import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferSeviceTest {

    private Transfer transfer;
    private Account account1;
    private Account account2;

    private User user1;
    private User user2;


    @InjectMocks
    private TransferSeviceImpl transferService;

    @Mock
    TransferRepository transferRepository;
    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    AccountRepository accountRepository;


    @BeforeEach
    void initialize(){
        user1 = new User();
        user1.setUsername("user1");
        user1.setLastname("last1");
        user1.setFirstname("first1");
        user1.setGender("Male");


        user2 = new User();
        user2.setUsername("user2");
        user2.setLastname("last2");
        user2.setFirstname("first2");
        user2.setGender("Female");


        account1 = new Account();
        account1.setId(1L);
        account1.setNrCompte("010000A000001000");
        account1.setRib("RIB1");
        account1.setSolde(BigDecimal.valueOf(20000));
        account1.setUser(user1);


        account2 = new Account();
        account2.setId(2L);
        account2.setNrCompte("010000B025001000");
        account2.setRib("RIB2");
        account2.setSolde(BigDecimal.valueOf(14000));
        account2.setUser(user2);


        transfer = new Transfer();
        transfer.setMotifTransfer("Rien");
        transfer.setMontantTransfer(BigDecimal.valueOf(2000));
        transfer.setCompteBeneficiaire(account2);
        transfer.setCompteEmetteur(account1);
        transfer.setDateExecution(new Date());

    }

    @AfterEach
    void destroy(){
        account1=null;
        account2=null;

        user1 =null;
        user2 =null;
    }

    @DisplayName("It Should raise an AccountNotFoundException since the sender account is not porvided")
    @Test
    public void testTransferSenderAccountNotFoundException(){

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Issuer account Non-existent");

    }

    @DisplayName("It Should raise an AccountNotFoundException since the reciever account is null")
    @Test
    public void testTransferRecieverAccountNotFoundException(){

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Receiver Account Non-existent");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is zero or null")
    @Test
    public void testTransferZeroMoneyException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(0));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Empty amount");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is less than the minimum value")
    @Test
    public void testTransferInsuffiscientMoneyException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Minimum transfer amount not reached");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is greater than the maximum barrier")
    @Test
    public void testTransferMaximumMoneyReachedException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2000000));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Maximum transfer amount exceeded");

    }


    @DisplayName("It Should raise an InsuffisantException since the sender's money is less than the transaction money")
    @Test
    public void testTransferInsuffisantBalanceException(){
        account1.setSolde(BigDecimal.valueOf(300));

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        InsuffisantBalanceException transactionException = assertThrows(InsuffisantBalanceException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Insufficient issuer balance");

    }


    @DisplayName("It Should raise an TransactionException since the transaction motif is not given")
    @Test
    public void testTransferTransactionMotifException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMotifTransfer(null);

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"invalid Reason");

    }


    @DisplayName("It Should reduce the amount of money transfered from the sender and add it to the reciever")
    @Test
    public void testTransferHasBeenSuccessfull(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        try {
            BigDecimal expectedAccount1Sold = account1.getSolde().subtract(transfer.getMontantTransfer());
            BigDecimal expectedAccount2Sold =account2.getSolde().add(transfer.getMontantTransfer());
            transferService.createTransfer(TransferMapper.map(transfer));
            assertEquals(expectedAccount1Sold,account1.getSolde());
            assertEquals(expectedAccount2Sold,account2.getSolde());

        } catch (TransactionException e) {
            throw new RuntimeException(e);
        } catch (InsuffisantBalanceException e) {
            throw new RuntimeException(e);
        } catch (AccountNotFoundException e) {
            throw new RuntimeException(e);
        }


    }



}